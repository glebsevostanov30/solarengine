package com.example.workflow.Parse;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public class fupParser {
    @JsonProperty
    private String[] definition_keys;

    public String[] getDefinition_keys() {
        return definition_keys;
    }

    public void setDefinition_keys(String[] definition_keys) {
        this.definition_keys = definition_keys;
    }

    @Override
    public String toString() {
        return "fupParser{" +
                "definition_keys=" + Arrays.toString(definition_keys) +
                '}';
    }
}
