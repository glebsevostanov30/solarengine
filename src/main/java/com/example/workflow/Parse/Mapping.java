package com.example.workflow.Parse;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
// главный класс маппинга
// который содержит массив Results
// и класс Info
// которые берутся с https://randomuser.me/api
// все остальны классы это модель
public class Mapping {
    @JsonProperty
    private Results[] results;
    @JsonProperty
    private Info info;

    public Results getResults() {
        return results[0];
    }

    public void setResults(Results[] results) {
        this.results = results;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Mapping{" +
                "results=" + Arrays.toString(results) +
                ", info=" + info.toString() +
                '}';
    }
}
