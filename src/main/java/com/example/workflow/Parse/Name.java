package com.example.workflow.Parse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Name {
    @JsonProperty
    public String title = "";
    @JsonProperty
    public String first = "";
    @JsonProperty
    public String last = "";

    public String getTitle() {
        return title;
    }
    public Name(){}
    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    @Override
    public String toString() {
        return "Name{" +
                "title='" + title + '\'' +
                ", first='" + first + '\'' +
                ", last='" + last + '\'' +
                '}';
    }
}
