package com.example.workflow.taskList;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.task.Task;

import java.util.List;

public class filterGender {

    private int count_female_teamA = 0;
    private int count_male_teamA = 0;
    private int count_female_teamB = 0;
    private int count_male_teamB = 0;


    public filterGender(){

    }
    public void TaskList(String team){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        TaskService taskService = processEngine.getTaskService();
        if(team.equals("TeamA") || team.equals("TeamX")) {
            List<Task> tasksMaleTeamA = taskService.createTaskQuery()
                    .processDefinitionKey("TeamA")
                    .processVariableValueEquals("gender", "male")
                    .list();
            count_male_teamA = (int) tasksMaleTeamA.stream().count();

            List<Task> tasksFemaleTeamA = taskService.createTaskQuery()
                    .processDefinitionKey(team)
                    .processVariableValueEquals("gender", "female")
                    .list();
            System.out.println(tasksFemaleTeamA);
            count_female_teamA = (int) tasksFemaleTeamA.stream().count();
        }

        if(team.equals("TeamB") || team.equals("TeamX")) {
            List<Task> tasksMaleTeamB = taskService.createTaskQuery()
                    .processDefinitionKey("TeamB")
                    .processVariableValueEquals("gender", "male")
                    .list();
            count_male_teamB = (int) tasksMaleTeamB.stream().count();

            List<Task> tasksFemaleTeamB = taskService.createTaskQuery()
                    .processDefinitionKey(team)
                    .processVariableValueEquals("gender", "female")
                    .list();
            count_female_teamB = (int) tasksFemaleTeamB.stream().count();
        }

    }

    public String toString(String team) {
        if (team.equals("TeamA")) {
            return "\"filterGender\"{" +
                    "\"count_female_teamA\" = " + count_female_teamA +
                    ", \"count_male_teamA\" = " + count_male_teamA +
                    '}';
        }

        if (team.equals("TeamB")) {
            return "\"filterGender\"{" +
                    "\"count_female_teamB\" = " + count_female_teamB +
                    ", \"count_male_teamB\" = " + count_male_teamB +
                    '}';
        }

        if (team.equals("TeamX")) {
            return "\"filterGender\"{" +
                    "\"count_female_teamA\" = " + count_female_teamA +
                    ", \"count_male_teamA\" = " + count_male_teamA +
                    ", \"count_female_teamB\" = " + count_female_teamB +
                    ", \"count_male_teamB\" = " + count_male_teamB +
                    '}';
        }

        return "Error";
    }
}
