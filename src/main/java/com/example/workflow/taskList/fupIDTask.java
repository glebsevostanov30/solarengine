package com.example.workflow.taskList;

import camundajar.impl.com.google.gson.Gson;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.camunda.spin.Spin.S;

public class fupIDTask {
    String json = "";
    private List<Task> TaskList;

    @Bean
    public void start_Task(String[] tasks) throws IOException {

        //@Autowired не работает в следующий раз постараюсь сделать)
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();

        //чтобы узнать количество элементов для массива ListID
        int size_mas = 0;

        //чтобы узнать без повторения кол-во definition keys и не добавлять в список
        Set<String> called = new HashSet<String>(Arrays.asList(tasks));

        //объект который будет помогать сериализовать
        Gson gson = new Gson();

        //прогон по всем таскам чтобы узнать количество для создания массива
        for(String task : called) {
            TaskList = taskService
                    .createTaskQuery()
                    .processDefinitionKey(task).list();
            size_mas += TaskList.stream().count();
        }

        //хранилище которое будет собирать все ID
        String[] ListID = new String[size_mas];

        //цикл для пробега по definition_keys
        for(String task : called){
            TaskList = taskService
                     .createTaskQuery()
                     .processDefinitionKey(task).list();
            //цикл для пробега по листу и достать от туда др объект
            for(int i = 0; i < TaskList.stream().count(); i++){
                //на всякий случай)
                try{
                    ListID[i] = TaskList.get(i).getId();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }
        //сериализация данных
        json = gson.toJson(ListID);
    }

    @Override
    public String toString() {
        return "{tasksID:" + json + "}";
    }
}


