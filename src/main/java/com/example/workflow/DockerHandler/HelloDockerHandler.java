package com.example.workflow.DockerHandler;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloDockerHandler {
    @GetMapping("/")
    public String hello() {
        return "Hello Docker!";
    }
}