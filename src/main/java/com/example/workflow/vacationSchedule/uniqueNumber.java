package com.example.workflow.vacationSchedule;

import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.history.HistoricCaseInstance;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.camunda.bpm.engine.history.HistoricVariableInstance;

import java.util.List;

public class uniqueNumber implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        HistoryService historyService = processEngine.getHistoryService();
        List<HistoricProcessInstance> asd1 = historyService.createHistoricProcessInstanceQuery().finished().list();
        System.out.println(asd1);

        List<HistoricCaseInstance> asd = historyService.createHistoricCaseInstanceQuery().closed().list();
        System.out.println(asd);

        List<HistoricVariableInstance> asd2 = historyService
                .createHistoricVariableInstanceQuery()
                .processDefinitionKey("request_vacation_schedule")
                .list();
        System.out.println(asd2);

        List<HistoricTaskInstance> asd3 = historyService
                .createHistoricTaskInstanceQuery()
                .finished()
                .taskAssignee("Gleb_manager").list();
        System.out.println(asd3);
    }
}
