package com.example.workflow.vacationSchedule;

public enum vacation {
    //Ежегодный оплачиваемый отпуск
    Value_38so99o("Value_38so99o"),
    //Отпуск без сохранения ЗП
    Value_3uu0016("Value_3uu0016"),
    //Отпуск по уходу за ребенком
    Value_033hd6n("Value_033hd6n");


    private final String str;
    vacation(String str) {
        this.str = str;
    }

    public String toString() {
        return str;
    }
}
