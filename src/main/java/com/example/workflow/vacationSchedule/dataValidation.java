package com.example.workflow.vacationSchedule;

import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.camunda.bpm.engine.history.HistoricVariableInstance;

import java.util.Date;
import java.util.List;

public class dataValidation implements JavaDelegate{

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        HistoryService historyService = processEngine.getHistoryService();
        IdentityService identityService = processEngine.getIdentityService();

        final long current = 86400000;
        final int all_days = 28;

        String typeofvacation = (String) execution.getVariable("typeofvacation");
        execution.setVariable("APP", "APP1");
        execution.setVariable("status", "Value_348tgq8");

        //имена переменных гениальны
        //переменные которые используются для истории
        Date start_date = null;
        Date end_date = null;
        //

        //дни которые 100% использовали
        int days_are_blocked = 0;
        String APP = "APP1";
        String user = "";
        String status = "";

        Date date_start = (Date) execution.getVariable("data_start");
        Date date_end = (Date) execution.getVariable("data_end");
        //некорректный ввод
        if(date_start == null || date_end == null){
            throw new Exception("Данные не введены");
        }
        //подсчёт сколько дней потребовал пользователь
        int daysUntil = (int) ((date_end.getTime() - date_start.getTime())/ current);

        //дн которые смогут использовать
        int days_used = 0;

        //простенькая проверка
        if(date_end.getTime() < date_start.getTime()){
            throw new Exception("Дата начала отпуска больше даты конца.");
        }

        //Ежегодный оплачиваемый отпуск
        //Проверка под буквой B
        if(typeofvacation.equals("Value_38so99o")  && daysUntil < 14){
            throw new Exception("Минимальное количество дней для такого отпуска - 14.");
        }

        //Отпуск без сохранения ЗП
        //проверка под буквой C
        if(typeofvacation.equals("Value_3uu0016") && daysUntil > 3){
            throw new Exception("Максимальное количество дней для такого отпуска - 3.");
        }

        //поднимаем историю
        List<HistoricTaskInstance> tasks = historyService
                .createHistoricTaskInstanceQuery()
                .finished()
                .processDefinitionKey("request_vacation_schedule")
                .list();

        //попробовать в будущем throw new ProcessEngineException


        for(HistoricTaskInstance task:tasks){
            List<HistoricVariableInstance> tasksVars = historyService.createHistoricVariableInstanceQuery().processInstanceId(task.getProcessInstanceId()).list();

            for(HistoricVariableInstance var:tasksVars){
                if(var.getName().equals("data_start"))
                    start_date = (Date) var.getValue();
                if(var.getName().equals("data_end"))
                    end_date = (Date) var.getValue();
                if(var.getName().equals("APP"))
                    APP = (String) var.getValue();
                if(var.getName().equals("starter"))
                    user = (String) var.getValue();
                if(var.getName().equals("status")){
                    status = (String)  var.getValue();
                }
            }


            //согласовано дней
            if(status.equals("Value_06o2go2")){
                days_are_blocked += (int) ((end_date.getTime() - start_date.getTime()) / current);
            }

            if(start_date!=null && end_date!=null && user.equals(identityService.getCurrentAuthentication().getUserId()) && status.equals("Value_348tgq8")) {

                days_used += (int) ((end_date.getTime() - start_date.getTime()) / current);

                //проверка под буквой D
                if(days_used + days_are_blocked < 28 && days_used + days_are_blocked + daysUntil > 28) {
                    throw new Exception(user + " осталось только " + (28 - days_are_blocked - days_used) + " дней отпуска.");
                }

                //Ежегодный оплачиваемый отпуск
                //проверка чтобы не взял больше чем надо изначально
                if(daysUntil > 28 && typeofvacation.equals("Value_38so99o")) {
                    throw new Exception("Превышено общее количество дней отпуска на : " + (daysUntil - (28 - days_used)));
                }

                //проверка на всякий запасной
                if(days_used > 28 || days_are_blocked > 28){
                    throw new Exception("Ошибка," + user + " исользовали все дни");
                }

                //выдает новый APP номер. Проверка под буквой E
                if(!APP.equals("") || APP != null){
                    execution.setVariable("APP", "APP" + (Integer.parseInt(APP.substring(3, APP.length())) + 1));
                }


            }else{
                //для того чтобы следующий if не поймал null
                start_date=null; end_date=null;
                //если же будет null, то он пропустит следующую проверку
                continue;
            }
            //проверка под буквой A
            if(start_date.getTime() <= date_start.getTime() && end_date.getTime() >= date_start.getTime() ||
                    start_date.getTime() <= date_end.getTime() && end_date.getTime() >= date_end.getTime()){
                throw new Exception("Уже существует заявка на указанный период отпуска. Пожалуйста, исправьте сроки");
            }

            start_date=null; end_date=null;
        }


        System.out.println(execution.getVariable("APP"));
        System.out.println("days_used " + days_used + days_are_blocked + daysUntil);
    }
}
