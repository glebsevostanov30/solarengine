package ru.di.bpm.camunda.core.systools.vareditior;

import org.camunda.bpm.engine.runtime.VariableInstance;

import java.util.List;

public interface VariableEditor {
    /**Список типов установки значения переменных
     * <ul>
    <li><b>SINGLE</b> - изменить ТОЛЬКО в конкретном процессе
     <li><b>TREE_FAMILY</b> - изменения касаются во всем древе процессов. Если переменной нет, то создать ее. Если переменная существует,
    то перезаписать.
     <li><b>TREE_FAMILY_SOFT</b> - изменения касаются во всем древе процессов. Изменить значение переменной только в том случае, если
    переменная существует.
     </ul>**/

    /**Метод устанавливает значение <b>valueVar</b> в переменную <b>nameVar</b> в процессе с id = <b>processInstanceId</b>.
    В ответ должно вернуться true, если метод успешный. False, если ошибка.**/

    public boolean setProcessVariable(String processInstanceId, String nameVar, Object valueVar, TypeProcessChange type);

    /**Эквивалентно методу выше, за исключением того, что по умолчанию, TypeProcessChange = TREE_FAMILY*/
    public boolean setProcessVariable(String processInstanceId, String nameVar, Object valueVar);

    /**Метод должен установить значение <b>valueVar</b> в локальную переменную <b>nameVar</b> таски с id = <b>processInstanceId</b>.
     В ответ должно вернуться true, если метод успешный. False, если ошибка.**/
    public boolean setTaskLocalVariable(String TaskId, String nameVar, Object valueVar);

    /**
     * Метод возвращает список переменных процесса по его ID.
     * **/
    public List<VariableInstance> getProcessVariables(String processInstanceId);

    /**
     * Метод возвращает переменную процесса по его ID и имени переменной.
     * **/
    public VariableInstance getProcessVariable(String processInstanceId, String nameVar);

    /**
     * Метод возвращает локальные переменные таски по ID таски.
     * **/
    public List<VariableInstance> getTaskLocalVariables(String TaskId);

    /**
     * Метод возвращает локальную переменную таски по ID таски и имени переменной.
     * **/
    public VariableInstance getTaskLocalVariable(String TaskId, String nameVar);


}
