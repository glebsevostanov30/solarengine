package ru.di.bpm.camunda.core.systools.vareditior;


import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.VariableInstance;
import org.camunda.bpm.engine.spring.annotations.ProcessVariable;
import org.camunda.bpm.engine.task.Task;

import java.util.List;

public enum TypeProcessChange implements Strategy {
    SINGLE{
        @Override
        public void execution(RuntimeService runtimeService, String processInstanceId, String nameVar, Object valueVar) {
            runtimeService.setVariable(processInstanceId, nameVar, valueVar);
        }
    },

    TREE_FAMILY{
        @Override
        public void execution(RuntimeService runtimeService, String processInstanceId, String nameVar, Object valueVar) {
            List<VariableInstance> variables = runtimeService.createVariableInstanceQuery()
                    .processInstanceIdIn(processInstanceId)
                    .variableName(nameVar).list();

        }
    },

    TREE_FAMILY_SOFT{
        @Override
        public void execution(RuntimeService runtimeService, String processInstanceId, String nameVar, Object valueVar) {
            List<VariableInstance> ProcessVariable =  runtimeService.createVariableInstanceQuery()
                    .processInstanceIdIn(processInstanceId)
                    .variableName(nameVar).list();
        }
    }

}
