package ru.di.bpm.camunda.core.systools.vareditior;

import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.Expression;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.runtime.VariableInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


@Component("Variable_editor")
public class Variable_editor implements VariableEditor {
    private final static Logger LOGGER = Logger.getLogger("Change property request");
    private String processName;
    private String processId;
    private String buisnessKey;
    private String parent;
    private String superRootPocessID = "";

    private HistoryService historyService;
    private ProcessEngine processEngine;
    private DelegateExecution superExecution;
    private List list_instances;

    private String property_current_val;
    private String property_val;
    private String property_name;

    Map<String, Object> variables = new HashMap<>();


    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    RuntimeService runtimeService;

    public void execution(DelegateExecution delegateExecution, Expression property_val, Expression property_name) throws Exception {
        //Установка родительского процесса
        setSuperRootProcessID(delegateExecution);

        //Чтение основных данных
        this.property_name = (String) property_name.getValue(delegateExecution);
        this.property_current_val = (String) runtimeService.getVariable(superRootPocessID, this.property_name);
        this.property_val = (String) property_val.getValue(delegateExecution);

        equalsProperty();

        setVariable(delegateExecution);
    }

    public void execution(DelegateExecution delegateExecution, String property_val, String property_name) throws Exception {
        //Чтение основных данных
        setSuperRootProcessID(delegateExecution);

        this.property_name = property_name;
        this.property_val = property_val;
        this.property_current_val = (String) runtimeService.getVariable(superRootPocessID, this.property_name);

        equalsProperty();

        setVariable(delegateExecution);
    }

    //Поиск родительского процесса
    private void setSuperRootProcessID(DelegateExecution delegateExecution) throws Exception{

        processEngine = delegateExecution.getProcessEngine();
        historyService = processEngine.getHistoryService();
        buisnessKey = delegateExecution.getBusinessKey();
        processId = delegateExecution.getProcessInstanceId();

        processName = delegateExecution.getProcessEngineServices()
                .getRepositoryService()
                .getProcessDefinition(delegateExecution.getProcessDefinitionId())
                .getKey();

        variables = delegateExecution.getVariables(); // <- больше нужно для отладки

        try{
            parent = delegateExecution.getParentId();
            if(parent == null){
                superExecution = delegateExecution.getSuperExecution();
                if(superExecution == null){
                    superRootPocessID = delegateExecution.getProcessInstanceId();
                }
                else {
                    superRootPocessID = delegateExecution.getSuperExecution().getProcessInstanceId();
                }
            }
            else{
                list_instances = historyService.createHistoricProcessInstanceQuery().processInstanceBusinessKey(buisnessKey).list();
                if(list_instances.size() == 0){
                    superRootPocessID = delegateExecution.getProcessInstanceId();
                }else{
                    superRootPocessID = ((HistoricProcessInstance) list_instances.get(0)).getId();
                }
            }
        }catch (Exception e){
            LOGGER.warning("Error in define parent process");
            throw new Exception("ChangePropertyInRootProcess: Ошибка при определении родительского процесса");
        }
    }

    @Override
    public boolean setProcessVariable(String processInstanceId, String nameVar, Object valueVar, TypeProcessChange type) {
        try {
            type.execution(runtimeService, processInstanceId, nameVar, valueVar);
        }catch (Exception e){
            LOGGER.info("Failed to set the variable in " + type);
        }

        return false;
    }

    @Override
    public boolean setProcessVariable(String processInstanceId, String nameVar, Object valueVar) {
        try {
            runtimeService.setVariable(processInstanceId, nameVar, valueVar);
            return true;
        }catch (Exception e){
            LOGGER.info("Failed to set the variable");
        }
        return false;
    }

    @Override
    public boolean setTaskLocalVariable(String TaskId, String nameVar, Object valueVar) {
        try {
            runtimeService.setVariableLocal(TaskId, nameVar, valueVar);
            return true;
        }catch (Exception e){
            LOGGER.info("Failed to set the local variable");
        }
        return false;
    }

    @Override
    public List<VariableInstance> getProcessVariables(String processInstanceId) {
        List<VariableInstance> ProcessVariables = runtimeService.createVariableInstanceQuery()
                .processInstanceIdIn(processInstanceId).list();
        return  ProcessVariables;
    }

    @Override
    public VariableInstance getProcessVariable(String processInstanceId, String nameVar) {
        VariableInstance ProcessVariable = (VariableInstance) runtimeService.createVariableInstanceQuery()
                .processInstanceIdIn(processInstanceId)
                .variableName(nameVar);
        return  ProcessVariable;
    }

    @Override
    public List<VariableInstance> getTaskLocalVariables(String TaskId) {
        List<VariableInstance> TaskLocalVariables = runtimeService.createVariableInstanceQuery().taskIdIn(TaskId).list();
        return  TaskLocalVariables;
    }

    @Override
    public VariableInstance getTaskLocalVariable(String TaskId, String nameVar) {

        VariableInstance TaskLocalVariable = (VariableInstance) runtimeService.createVariableInstanceQuery().taskIdIn(TaskId).variableName(nameVar);
        return TaskLocalVariable;
    }

    public void equalsProperty(){
        if(property_current_val == null) {property_current_val = "";}
        if(property_val == null) {property_val = "";}

        if(property_val.toLowerCase().equals(property_current_val.toLowerCase())){
            LOGGER.warning("New property equals old value");
        }
    }

    private void setVariable(DelegateExecution delegateExecution){
        runtimeService.setVariable(superRootPocessID, property_name, property_val);
        runtimeService.setVariable(delegateExecution.getProcessInstanceId(), property_name, property_val);
        LOGGER.info("Property ["+property_name+"] changed to ["+property_val+"] successful in ["+processName+"]");
    }
}


