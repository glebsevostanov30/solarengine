package ru.di.bpm.camunda.core.systools.vareditior;

import org.camunda.bpm.engine.RuntimeService;

public interface Strategy {
    public void execution(RuntimeService runtimeService, String processInstanceId, String nameVar, Object valueVar);
}
