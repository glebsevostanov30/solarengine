package com.example.workflow.Process;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

//ручка для создания 100 процессов

@RestController
public class TeamController {
    Team TeamX = new Team();

    @PostMapping(value = "/camunda/rest-api/Team/{count}", produces = "application/json")
    public ResponseEntity<?> getEmployeeInJSON(@PathVariable int count) {
        TeamX.startTeam(count);
        return new ResponseEntity<>(TeamX.toString(), HttpStatus.OK);
    }
}
