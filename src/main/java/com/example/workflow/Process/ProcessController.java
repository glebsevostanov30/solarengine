package com.example.workflow.Process;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

//ручка для определения, кто вызвал процесс

@RestController
public class ProcessController {
    private Map<String, String> env = System.getenv();
    Process process = new Process();

    @GetMapping(value = "/camunda/rest-api/StartProcessID/{key}", produces = "application/json")
    public ResponseEntity<?> getProcess(@PathVariable String key) {
        process.startProcess(key);
        return new ResponseEntity<>(process.toString(), HttpStatus.CREATED);
    }

}