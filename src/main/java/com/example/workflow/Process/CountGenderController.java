package com.example.workflow.Process;

import com.example.workflow.taskList.filterGender;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountGenderController {
    filterGender filter = new filterGender();
    @PostMapping(value = "/camunda/rest-api/calc", produces = "application/json")
    public ResponseEntity<?> getProcess(@RequestParam String team) {
        filter.TaskList(team);
        return new ResponseEntity<>(filter.toString(team), HttpStatus.OK);
    }
}
