package com.example.workflow.Process;

import com.example.workflow.Parse.fupParser;
import com.example.workflow.taskList.fupIDTask;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import static org.camunda.spin.Spin.JSON;

@RestController
public class fupController {
    fupIDTask start = new fupIDTask();
    //definition_keys[] mapping = new definition_keys[10];

    @ResponseBody
    @PostMapping(value = "/camunda/rest-api/task", produces = "application/json")
    public ResponseEntity<?> getEmployeeInJSON(@RequestBody String task) throws IOException {
        fupParser mapping = JSON(task).mapTo(fupParser.class);
        start.start_Task(mapping.getDefinition_keys());
        return new ResponseEntity<>(start.toString(), HttpStatus.OK);
    }
}
