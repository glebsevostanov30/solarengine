package com.example.workflow.Process;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.springframework.context.annotation.Bean;

//класс который создаст 50 процессов TeamA
//и 50 процессов TeamB

public class Team {

    @Bean
    public void startTeam(int count){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        for(int i = 0; i< count; i++){
            processEngine.getRuntimeService().startProcessInstanceByKey("TeamA");
            processEngine.getRuntimeService().startProcessInstanceByKey("TeamB");
        }
    }

}
