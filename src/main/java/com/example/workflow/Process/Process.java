package com.example.workflow.Process;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;



public class Process {
    
    @Bean
    public void startProcess (String key){


        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("source", "api");
        RuntimeService runtimeService = ProcessEngines.getDefaultProcessEngine().getRuntimeService();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(key);

    }

    @Override
    public String toString() {
        return "{\"Process\"{\"source\" = \"api\"}}";
    }
}