package com.example.workflow;

import com.example.workflow.Parse.Mapping;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Map;

import static org.camunda.spin.Spin.JSON;

public class GeneratePerson implements JavaDelegate {
    private Map<String, String> env = System.getenv();
    //servis task, который генерирует человека
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        //маппит
        Mapping resa = JSON(Request()).mapTo(Mapping.class);
        //создает новую персону
        delegateExecution.setVariable("firstName", resa.getResults().getName().getTitle());
        delegateExecution.setVariable("lastName" , resa.getResults().getName().getLast());
        delegateExecution.setVariable("gender", resa.getResults().getGender());
    }

    // функция для отправки get запроса на ссылку
    public String Request() {
        String url = env.get("GENERATE_RANDOM_PERSON");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        final String response = restTemplate.getForObject(url, String.class);

        return response;
    }

}
