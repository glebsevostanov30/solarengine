FROM adoptopenjdk/openjdk13
EXPOSE 8080
WORKDIR /apps
RUN apt-get update && apt-get install
COPY /target/internship.jar  /apps/app.jar
COPY /.env  /apps/.env
ENTRYPOINT ["java", "-jar", "/apps/app.jar"]


