#Стажировка
#WEEK1
Ручка для создания новой таски (уже задеплоенной)
##GET
 >/camunda/rest-api/StartProcessID/{key}

В переменную source кладет значение *api*, при вызове через ручку или если вызывалось через админку, то *adminka*
#WEEK2
##POST
>/camunda/rest-api/Team/{count}

count - это кол-во вызываемых тасок 

##POST
>/camunda/rest-api/calc

Ручка, способная фильтровать процессы по их processDefinitionKey, но и то не все
а только TeamA, TeamB, TeamX.
Где TeamA - вывидит количество гендеров этой команды,
TeamB - вывидит количество гендеров этой команды,
TeamX - кол-во тех и тех.
#WEEK3
Добавлена возможность контейнеризации. Для запуска необходимо прописать 

>mvn package

>docker build -t image:1.2 .

>docker run --env-file .env -p 8080:8080 image:1.2

Где image - это название вашего образа.

#Выполнено доп задание
##POST
>/camunda/rest-api/task
##BODY
>{
>"definition_keys":["id1", "id2", "id3"]
>}

Где id - это id диаграммы в админке 